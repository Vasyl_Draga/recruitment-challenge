//
//  Resource.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

enum Status {
    case SUCCESS
    case ERROR
    case LOADING
    case FINISH
}

struct Resource<T> {
    let data: T?
    let status: Status
    let message: String
}
