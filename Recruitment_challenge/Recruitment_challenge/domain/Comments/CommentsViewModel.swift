//
//  CommentsViewModel.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import RxSwift

class CommentsViewModel {
    private var start: Int = 0
    private var end: Int = 0
    private var maxCommentsOnScreen: Int = 0
    private var disposeBag = DisposeBag()
    private var model: CommentsModel
    private var data: PublishSubject<Resource<[Comment]>>
    private var loadedList: [Comment] = []
    private let initialResource = Resource<[Comment]>(
        data: [],
        status: Status.LOADING,
        message: "Loading"
    )
    
    convenience init() {
        self.init(start: 0,
                  end: 0)
    }
    
    convenience init(start: String?,
                     end: String?) {
        if let lower = Int(start ?? "0"),
            let upper = Int(end ?? "0") {
            self.init(start: lower,
                      end: upper)
        } else {
            self.init(start: 0,
                      end: 0)
        }
    }
    
    init(start: Int,
         end: Int) {
        self.model = CommentsModel()
        self.data = PublishSubject<Resource<[Comment]>>()
        self.start = start - 1
        self.end = end
    }
    
    func getData(maxCommentsOnScreen: Int) -> PublishSubject<Resource<[Comment]>> {
        self.maxCommentsOnScreen = maxCommentsOnScreen
        return self.data
    }
    
    func fetchData() {
        self.fetchComments()
        self.load()
    }
    
    func load() {
        self.model.loadComments(start: self.calculateStart(),
                                amount: self.calculateAmount())
    }
    
    func cancel() {
        self.model.cancel()
    }
    
    private func calculateStart() -> Int {
        return self.start + self.loadedList.count
    }
    
    private func fetchComments() {
        self.model.observableComments()
            .observeOn(MainScheduler.instance)
            .do(onNext: { (comments) in
                self.handleSuccess(comments: comments)
            }, onError: { (error) in
                self.handleFailure(error: error)
            }, onCompleted: {
                self.handleComplete()
            }, onSubscribed: {
                self.data.onNext(self.initialResource)
            })
            .subscribe()
            .disposed(by: self.disposeBag)
    }
    
    private func calculateAmount() -> Int {
        let unloaded = self.end - self.loadedList.count - self.start
        return unloaded > self.maxCommentsOnScreen ?
            self.maxCommentsOnScreen :
        unloaded
    }
    
    private func handleSuccess(comments: [Comment]) {
        if comments.count != 0 {
            self.loadedList.append(contentsOf: comments)
            self.data.onNext(Resource<[Comment]>(
                data: self.loadedList,
                status: Status.SUCCESS,
                message: "Loaded comments"
            ))
        }
    }
    
    private func handleComplete() {
        self.data.onNext(Resource<[Comment]>(
            data: nil,
            status: Status.FINISH,
            message: "All comments are loaded"
        ))
        self.data.onCompleted()
    }
    
    private func handleFailure(error: Error) {
        self.data.onNext(Resource<[Comment]>(
            data: nil,
            status: Status.ERROR,
            message: "Failed to load restaurants: \(error.localizedDescription)"
        ))
    }
}
