//
//  CommentsModel.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import RxSwift

class CommentsModel {
    private let repository: CommentsRepository
    
    init() {
        self.repository = CommentsRepository()
    }
    
    func observableComments() -> BehaviorSubject<[Comment]> {
        return self.repository.observableComments()
    }
    
    func loadComments(start: Int,
                      amount: Int) {
        self.repository.loadComments(start: start,
                                     amount: amount)
    }
    
    func cancel() {
        self.repository.cancel()
    }
}
