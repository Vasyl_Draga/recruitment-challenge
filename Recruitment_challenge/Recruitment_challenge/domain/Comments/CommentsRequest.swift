//
//  CommentsRequest.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

protocol CommentsAPI {
    func observableComments() -> BehaviorSubject<[Comment]>
    func loadComments(start: Int,
                      amount: Int)
    func cancel()
}

class CommentsRequest: CommentsAPI {
    private let url = "\(RestAPI.baseURL)/comments"
    private var commentsList: BehaviorSubject<[Comment]> = BehaviorSubject<[Comment]>(value: [])
    
    func observableComments() -> BehaviorSubject<[Comment]> {
        return self.commentsList
    }
    
    func loadComments(start: Int,
                      amount: Int) {
        let commentsURL = self.url + "?_start=\(start)&_limit=\(amount)"
        Alamofire.request(commentsURL).responseJSON(completionHandler: { (response) in
            if let value = response.result.value,
                let comments = Mapper<Comment>().mapArray(JSONObject: value) {
                if comments.count == 0 {
                    self.commentsList.onCompleted()
                } else {
                    self.commentsList.onNext(comments)
                }
            } else if let error = response.result.error {
                self.commentsList.onError(error)
            }
        })
    }
    
    func cancel() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
}
