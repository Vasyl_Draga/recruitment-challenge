//
//  CommentsRepository.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import RxSwift

class CommentsRepository {
    private let request: CommentsAPI = CommentsRequest()
    
    func observableComments() -> BehaviorSubject<[Comment]> {
        return self.request.observableComments()
    }
    
    func loadComments(start: Int,
                      amount: Int) {
        self.request.loadComments(start: start,
                                  amount: amount)
    }
    
    func cancel() {
        self.request.cancel()
    }
}
