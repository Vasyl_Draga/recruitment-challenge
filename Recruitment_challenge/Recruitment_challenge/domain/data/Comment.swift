//
//  Comment.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import ObjectMapper

class Comment: Mappable {
    var postId: Int?
    var id: Int?
    var name: String?
    var email: String?
    var body: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        self.postId <- map["postId"]
        self.id <- map["id"]
        self.name <- map["name"]
        self.email <- map["email"]
        self.body <- map["body"]
    }
}
