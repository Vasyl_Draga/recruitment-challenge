//
//  ProgressView.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

protocol ProgressView {
    func stopProgress()
    func startProgress()
    func initProgress()
    func addProgress()
    func removeProgress()
    func showProgress()
    func hideProgress()
}
