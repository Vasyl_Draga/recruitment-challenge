//
//  RangeValidation.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

class RangeValidation: Validation {
    private let loverValue: String
    private let upperValue: String
    
    init(loverValue: String?,
         upperValue: String?) {
        self.loverValue = loverValue ?? ""
        self.upperValue = upperValue ?? ""
    }
    func isValid() -> Bool {
        var result = false
        if let upper = Int(self.upperValue),
            let lover = Int(self.loverValue),
            lover > 0 {
            result = lover < upper
        }
        return result
    }
}
