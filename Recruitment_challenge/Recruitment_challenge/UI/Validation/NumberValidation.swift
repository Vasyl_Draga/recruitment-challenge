//
//  NumberValidation.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

class NumberValidation: Validation {
    private let text: String
    private let digits = "0123456789"
    
    init(text: String?) {
        self.text = text ?? ""
    }
    
    func isValid() -> Bool {
        let aSet = NSCharacterSet(charactersIn: self.digits).inverted
        let compSepByCharInSet = self.text.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return self.text == numberFiltered
    }
}
