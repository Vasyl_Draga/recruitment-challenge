//
//  Validation.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/14/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation

protocol Validation {
    func isValid() -> Bool
}
