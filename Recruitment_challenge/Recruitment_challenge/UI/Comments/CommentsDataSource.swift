//
//  CommentsDataSource.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

extension CommentsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var result = UITableViewCell()
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.CELL_IDENTIFIER) as? CommentCell {
            cell.bind(comment: self.comments[indexPath.row])
            result = cell
        }
        return result
    }
}
