//
//  CommentCell.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet var name: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var message: UILabel!
    @IBOutlet var id: UILabel!
    
    func bind(comment: Comment) {
        self.name.text = "name: " + (comment.name ?? "")
        self.email.text = "email: " + (comment.email ?? "")
        self.message.text = comment.body
        if let id = comment.id {
            self.id.text = "id: \(id)"
        }
    }
    
    override func prepareForReuse() {
        self.clear()
    }
    
    func clear() {
        self.name.text = ""
        self.email.text = ""
        self.message.text = ""
    }
}
