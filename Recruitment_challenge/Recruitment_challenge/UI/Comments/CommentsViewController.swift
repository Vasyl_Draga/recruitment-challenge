//
//  CommentsViewController.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import RxSwift
import UIKit

class CommentsViewController: UIViewController {
    var viewModel: CommentsViewModel?
    var canceled: Bool = false
    weak var homeVC: HomeViewController?
    internal var comments: [Comment] = []
    internal let CELL_IDENTIFIER = "CommentCell"
    private let MAX_COMMENTS_AMOUNT_ON_SCREEN = 10
    private let DEADLINE: Int = 3
    private let disposeBag = DisposeBag()
    private let defaultLoadingMessage = "Loading"
    private let defaultFinishMessage = "Finish loading"
    private var count: Int = 0
    private var loadedCount: Int = 0 {
        didSet {
            if self.tableView != nil,
                self.loadedCount != 0 {
                self.addRows()
            }
        }
    }
    private var pastTime: Bool = false
    private var loaded: Bool = false {
        didSet {
            if self.pastTime,
                !self.canceled {
                self.homeVC?.present()
            }
        }
    }
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.addInfiniteScroll { (tableView) -> Void in
            self.count = self.comments.count
            self.viewModel?.load()
        }
    }
    
    func cancel() {
        self.canceled = true
        self.viewModel?.cancel()
    }
    
    func observData() {
        self.sendRequest()
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            self.pastTime = true
            if self.loaded,
                !self.canceled {
                self.homeVC?.present()
            }
        })
    }
    
    private func addRows() {
        let indexPaths = (self.count..<self.loadedCount).map { return IndexPath(row: $0, section: 0) }
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: indexPaths, with: .automatic)
        self.tableView.endUpdates()
        self.tableView.finishInfiniteScroll()
    }
    
    private func sendRequest() {
        _ = self.viewModel?.getData(maxCommentsOnScreen: self.MAX_COMMENTS_AMOUNT_ON_SCREEN)
            .subscribe({ (dataResource) in
                switch dataResource.element?.status {
                case .LOADING?:
                    self.handleLoading(message: dataResource.element?.message)
                case .SUCCESS?:
                    if let comments = dataResource.element?.data {
                        self.handleSuccess(data: comments)
                    }
                case .ERROR?:
                    self.handleError(dataResource.error)
                case .FINISH?:
                    self.handleFinish(message: dataResource.element?.message)
                case .none:
                    print("Unknown error appeared")
                }
            }).disposed(by: self.disposeBag)
        self.viewModel?.fetchData()
    }
    
    private func handleFinish(message: String?) {
        if let loadingMessage = message {
            print(loadingMessage)
        } else {
            print(self.defaultFinishMessage)
        }
        if self.tableView != nil {
            self.tableView.removeInfiniteScroll()
        } else {
            if !self.loaded {
                self.loaded = true
            }
        }
    }
    
    private func handleLoading(message: String?) {
        if let loadingMessage = message {
            print(loadingMessage)
        } else {
            print(self.defaultLoadingMessage)
        }
    }
    
    private func handleError(_ error: Error?) {
        print("Error")
        self.homeVC?.handleError(error)
    }
    
    private func handleSuccess(data: [Comment]) {
        if !self.loaded {
            self.loaded = true
        }
        self.comments = data
        self.loadedCount = data.count
    }
}
