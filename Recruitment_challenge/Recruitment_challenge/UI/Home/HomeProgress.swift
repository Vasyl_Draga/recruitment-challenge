//
//  HomeSpinner.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController: ProgressView {
    internal func stopProgress() {
        self.activityIndicator.stopAnimating()
    }
    
    internal func startProgress() {
        self.activityIndicator.startAnimating()
    }
    
    internal func initProgress() {
        self.activityIndicator.center = CGPoint(x: self.view.bounds.size.width/2,
                                                y: self.view.bounds.size.height/2)
        self.activityIndicator.color = .black
        self.addProgress()
    }
    
    internal func addProgress() {
        self.view.addSubview(self.activityIndicator)
    }
    
    internal func removeProgress() {
        self.activityIndicator.removeFromSuperview()
    }
    
    internal func showProgress() {
        self.contentView.alpha = 0.2
        self.cancelButton.isHidden = false
    }
    
    internal func hideProgress() {
        self.contentView.alpha = 1.0
        self.cancelButton.isHidden = true
    }
}
