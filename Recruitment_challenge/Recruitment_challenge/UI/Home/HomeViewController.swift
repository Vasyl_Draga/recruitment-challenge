//
//  ViewController.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class HomeViewController: UIViewController {
    private let ERROR_TITLE = "Error!"
    private let ERROR_MESSAGE = "Something went wrong! Please, check your input data."
    private let OK_TITLE = "OK"
    private let COMMENTS_IDENTIFIER = "CommentsViewController"
    private let COMMENTS_STORYBOARD = "CommentsStoryboard"
    private weak var comments: CommentsViewController?
    internal let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    @IBOutlet var loverNumber: UITextField!
    @IBOutlet var upperNumber: UITextField!
    @IBOutlet var contentView: UIView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    
    @IBAction func sendPressed(_ sender: Any) {
        if RangeValidation(loverValue: self.loverNumber.text,
                           upperValue: self.upperNumber.text).isValid() {
            self.applyEditing(isEnabled: false)
            self.showComments()
        } else {
            self.showError()
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.cancel()
        self.comments?.cancel()
    }
    
    func present() {
        if let commentsViewController = self.comments {
            self.applyEditing(isEnabled: true)
            self.present(commentsViewController,
                         animated: true,
                         completion: self.stopProgress)
        }
    }
    
    func handleError(_ error: Error?) {
        if let strongError = error {
            print(strongError.localizedDescription)
        }
        self.removeProgress()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initProgress()
        self.addTargets()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.hideProgress()
    }
    
    private func showError() {
        let alert = UIAlertController(title: self.ERROR_TITLE,
                                      message: self.ERROR_MESSAGE,
                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: self.OK_TITLE,
                               style: .default,
                               handler: nil)
        alert.addAction(ok)
        self.present(alert,
                     animated: true,
                     completion: nil)
    }
    
    private func showComments() {
        self.showProgress()
        self.initCommentsViewController()
        self.startProgress()
    }
    
    private func initCommentsViewController() {
        let storyboard = UIStoryboard(name: self.COMMENTS_STORYBOARD,
                                      bundle: nil)
        var result = CommentsViewController()
        if let viewController = storyboard.instantiateViewController(withIdentifier: self.COMMENTS_IDENTIFIER) as? CommentsViewController {
            self.configureComments(viewController)
            result = viewController
        }
        self.comments = result
    }
    
    private func configureComments(_ viewController: CommentsViewController) {
        viewController.viewModel = CommentsViewModel(start: self.loverNumber.text,
                                                     end: self.upperNumber.text)
        viewController.homeVC = self
        viewController.observData()
    }
    
    private func cancel() {
        self.hideProgress()
        self.stopProgress()
        self.applyEditing(isEnabled: true)
    }
    
    private func applyEditing(isEnabled: Bool) {
        self.upperNumber.isEnabled = isEnabled
        self.loverNumber.isEnabled = isEnabled
    }
    
    @IBAction func toHome(_ segue: UIStoryboardSegue) {
    }
}
