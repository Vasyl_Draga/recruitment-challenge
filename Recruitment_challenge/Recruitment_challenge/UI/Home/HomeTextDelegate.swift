//
//  HomeTextDelegate.swift
//  Recruitment_challenge
//
//  Created by Mac on 1/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let validation = NumberValidation(text: string)
        return validation.isValid()
    }
    func addTargets() {
        self.loverNumber.addTarget(self,
                                   action: #selector(HomeViewController.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
        self.upperNumber.addTarget(self,
                                   action: #selector(HomeViewController.textFieldDidChange(_:)),
                                   for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if self.loverNumber.text != "",
            self.upperNumber.text != "" {
            self.sendButton.isEnabled = true
        } else {
            self.sendButton.isEnabled = false
        }
    }
}
